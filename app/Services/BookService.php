<?php 

namespace App\Services;

use App\Traits\ConsumesExternalService;

class BookService {

    public $baseUrl;

    public function __construct(){
        $this->baseUrl = config('services.books.base_uri');
    }

}